image: lorisleiva/laravel-docker:latest

stages:
- build
- test
- deploy

# This job will initialise the application
composer:
  # During the build stage we will build all of the requirements for testing the app
  stage: build

  # Install all of the project's dependencies, copy the .env file and run generate the app's key
  script:
  - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
  - cp .env.example .env
  - php artisan key:generate

  # Store the artifact for a month, after the period's end we will not be able to download them via the UI anymore.
  artifacts:
    expire_in: 1 month
    paths:
    - vendor/
    - .env

  cache:
    # We key the cache using the branch's unique identifier using 'composer' suffix on the key
    key: ${CI_COMMIT_REF_SLUG}-composer
    # We only want to cache the vendor folder.
    paths:
    - vendor/

# Build all of the node.js dependencies and build all of our assets
npm:
  stage: build

  # Install node.js dependencies and build assets in production mode
  script:
  - npm install
  - npm run production

  # Store the artifact for a month, after the period's end we will not be able to download them via the UI anymore.
  artifacts:
    expire_in: 1 month
    paths:
    - node_modules
    - public/css
    - public/js

  # Cache the 'node_modules' folder using the 'npm' suffix on the key
  cache:
    key: ${CI_COMMIT_REF_SLUG}-npm
    paths:
    - node_modules/

# This job will run the laravel tests
phpunit:
  # This is the test stage, jobs within the same stage are run in parallel.
  stage: test

  # List of jobs from which it will download the artifacts
  dependencies:
  - composer

  # This is the list of commands to be executed by the phpunit job
  script:
  - phpunit --coverage-text --colors=never

# This job will check the project's code style and make sure it follows the standards.
codestyle:
  # We will run this job in parallel with the phpunit
  stage: test
  # This job does not require any cache since we're not running composer
  cache: {}
  # We'll comply with the PSR2 standard and check all .php file in our app folder.
  script:
  - phpcs --standard=PSR2 --extensions=php app

